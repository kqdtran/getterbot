#!/usr/bin/python -tt
# Have a conversation with a PandaBot AI
# Original Author A.Roots
# Modified and Ported to Python3 by Khoa Tran

from urllib.request import urlopen, Request
from urllib.parse import urlencode
import sys
from xml.dom import minidom
from xml.sax.saxutils import unescape

def main():
  human_input = input('You: ')
  if human_input == 'exit' or human_input == 'quit':
    sys.exit(0)
  
  base_url = 'http://www.pandorabots.com/pandora/talk-xml'
  data = urlencode([('botid', 'ebbf27804e3458c5'), ('input', human_input)])
  data = bytearray(data, 'utf-8')
  
  # Submit POST data and download response XML
  req = Request(base_url)
  fd = urlopen(req, data)
  
  # Take Bot's response out of XML
  xmlFile = fd.read()
  dom = minidom.parseString(xmlFile)
  objektid = dom.getElementsByTagName('that')
  bot_response = objektid[0].toxml()
  bot_response = bot_response[6:]
  bot_response = bot_response[:-7]

  # Some nasty unescaping
  bot_response = unescape(bot_response, {"&apos;": "'", "&quot;": '"'})

  # Print out the response of bot
  print ('Bot:', str(bot_response))
  
  # Repeat until terminated
  while True:
    main()

if __name__ == '__main__':
  print ('Hi. You can now talk to Getter. Type "exit" when done.')
  main()
